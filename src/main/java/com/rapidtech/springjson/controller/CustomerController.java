package com.rapidtech.springjson.controller;

import com.rapidtech.springjson.model.CustomerModel;
import com.rapidtech.springjson.model.CustomerRequestModel;
import com.rapidtech.springjson.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/customers")
public class CustomerController {
    private CustomerService service;

    @Autowired
    public CustomerController(CustomerService service){
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<Object> save(@RequestBody CustomerRequestModel request){
        return ResponseEntity.ok().body(service.saveAll(request));
    }

///    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
///   public ResponseEntity<Object> saveCustomer(@RequestBody CustomerRequestModel request){
//        Optional<CustomerRequestModel> result = cusService.save(request);
//        return ResponseEntity.ok().body(result);
//    }
}
