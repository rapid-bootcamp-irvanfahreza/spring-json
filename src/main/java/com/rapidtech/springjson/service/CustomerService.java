package com.rapidtech.springjson.service;

import com.rapidtech.springjson.model.CustomerModel;
import com.rapidtech.springjson.model.CustomerRequestModel;
import com.rapidtech.springjson.model.CustomerResponse;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<CustomerModel> getAll();

    CustomerResponse saveAll(CustomerRequestModel request);

    Optional<CustomerModel> save(CustomerModel model);

    Optional<CustomerModel> getById(Long id);

    Optional<CustomerModel> update(Long id, CustomerModel model);

    Optional<CustomerModel> delete(Long id);
}
