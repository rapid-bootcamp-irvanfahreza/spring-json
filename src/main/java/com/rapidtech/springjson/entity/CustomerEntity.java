package com.rapidtech.springjson.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rapidtech.springjson.model.AddressModel;
import com.rapidtech.springjson.model.CustomerModel;
import com.rapidtech.springjson.model.SchoolModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer_tab")
public class CustomerEntity{
    @Id
    @TableGenerator(name = "customer_id_generator", table = "sequence_tab",
            pkColumnName = "gen_name", valueColumnName = "gen_value",
            pkColumnValue="customer_id", initialValue=0, allocationSize=0)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "customer_id_generator")
    private Long id;
    @Column(name = "fullName", length = 20)
    private String fullName;
    @JsonIgnore
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<AddressEntity> address = new ArrayList<>();
    @JsonIgnore
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL)
    private List<SchoolEntity> schools = new ArrayList<>();
    @Column(name = "gender", length = 20)
    private String gender;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "dateOfBirth")
    private Date dateOfBirth;
    @Column(name = "placeOfBirth", length = 20)
    private String placeOfBirth;
//    @ManyToOne
//    @JoinColumn(name = "customer_req_id", insertable = false, nullable = false)

    public CustomerEntity(CustomerModel model){
        BeanUtils.copyProperties(model, this);
    }
//
    public CustomerEntity(Long id, String fullName, String gender) {
        this.id = id;
        this.fullName = fullName;
//        this.subAddress = getSubAddress();
        this.gender = gender;
///        this.dateOfBirth = getDateOfBirth();
//        this.placeOfBirth = getPlaceOfBirth();
    }

    public void addAddress(AddressEntity addressEntity){
        this.address.add(addressEntity);
        addressEntity.setCustomer(this);
    }

    public void addAddressList(List<AddressModel> address){
        for(AddressModel item: address){
            this.addAddress(new AddressEntity(item));
        }
    }

    public void addSchool(SchoolEntity schoolEntity){
        this.schools.add(schoolEntity);
        schoolEntity.setCustomer(this);
    }

    public void addSchoolList(List<SchoolModel> school){
        for(SchoolModel item: school){
            this.addSchool(new SchoolEntity(item));
        }
    }
}
