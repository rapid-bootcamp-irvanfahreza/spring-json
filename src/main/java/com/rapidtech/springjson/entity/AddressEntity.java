package com.rapidtech.springjson.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rapidtech.springjson.model.AddressModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "address_tab")
public class AddressEntity {
    @Id
    @TableGenerator(name = "address_id_generator", table = "sequence_tab",
            pkColumnName = "gen_name", valueColumnName = "gen_value",
            pkColumnValue="address_id", initialValue=0, allocationSize=0)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "address_id_generator")
    private Long id;
    @Column(name = "customer_id", insertable = false, updatable = false)
    private Long customerId;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private CustomerEntity customer;
    @Column(name = "name", length = 20, nullable = false)
    private String name;
///    @Column(name = "address", length = 50, nullable = false)
//    private String address;
    @Column(name = "village", length = 20, nullable = false)
    private String village;
    @Column(name = "district", length = 20, nullable = false)
    private String district;
    @Column(name = "city", length = 20)
    private String city;
    @Column(name = "province", length = 20, nullable = false)
    private String province;

    public AddressEntity (AddressModel model){
        BeanUtils.copyProperties(model, this);
    }

///    public AddressEntity(AddressModel model){
//        this.name = model.getName();
//        this.address = model.getAddress();
//        this.village = model.getVillage();
//        this.district = model.getDistrict();
//        this.city = model.getCity();
//        this.province = model.getProvince();
//    }
}
