package com.rapidtech.springjson.service.Impl;

import com.rapidtech.springjson.entity.AddressEntity;
import com.rapidtech.springjson.entity.CustomerEntity;
import com.rapidtech.springjson.entity.SchoolEntity;
import com.rapidtech.springjson.model.AddressModel;
import com.rapidtech.springjson.model.CustomerModel;
import com.rapidtech.springjson.model.SchoolModel;
import com.rapidtech.springjson.repository.CustomerRepo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Slf4j
@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {
    @InjectMocks
    @Autowired
    private CustomerServiceImpl service;

    @Mock
    private CustomerRepo repo;

    private List<CustomerEntity> customerEntityList;

    @BeforeEach
    void setUp() {
        log.info("Setup Run...");
        customerEntityList = Arrays.asList(
                new CustomerEntity(1L, "Reza", "Lelaki"),
                new CustomerEntity(2L, "Irvan", "Laki juga"),
                new CustomerEntity(3L, "Fah", "Laki-laki")
        );

    }

    @AfterEach
    void tearDown() {
        log.info("Setup Clear...");
    }

    @Test
    void getAll() {
        // Mocking adalah perumpamaan/pura-pura/ validasi(?)
        // ketika ada request repo.findAll, maka return value list kosong
        when(this.repo.findAll()).thenReturn(Collections.emptyList());
        List<CustomerModel> result = service.getAll();
        assertNotNull(result);
        assertEquals(0, result.size());

        when(this.repo.findAll()).thenReturn(customerEntityList);
        result = service.getAll();
        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals(2L, result.get(1).getId());
        assertEquals("Reza", result.get(0).getFullName());
        assertEquals("Laki-laki", result.get(2).getGender());

    }

    @Test
    void saveAll() {
    }

    @Test
    void getById() {
        Optional<CustomerModel> result = service.getById(0L);
        assertEquals(Optional.empty(), result);

        CustomerEntity customerEntity = new CustomerEntity(1L, "Irvan Fahreza", "Pria");
        Optional<CustomerEntity> optional = Optional.of(customerEntity);

        // proses mocking
        when(repo.findById(1L)).thenReturn(optional);
        result = service.getById(1L);

        assertTrue(result.isPresent());
        assertEquals(1L, result.get().getId());
        assertEquals("Irvan Fahreza", result.get().getFullName());
        assertEquals("Pria", result.get().getGender());
    }

    @Test
    void save() {
        Optional<CustomerModel> result = this.service.save(null);
        assertEquals(Optional.empty(), result);
        List<AddressModel> addressModels = Arrays.asList(
                new AddressModel(1L, "Address 1", "Jagakarsa", "Lenteng Agung", "Jakarta", "Jakarta Selatan"),
                new AddressModel(2L, "Address 2", "Grogol", "Daan Mogot", "Jakarta", "Jakarta Barat")
        );
        List<SchoolModel> schoolModels = Arrays.asList(
                new SchoolModel (1L, "SD", "SD 154", "SD Negeri"),
                new SchoolModel(2L, "SMP", "SMP MTS", "MTS Negeri"),
                new SchoolModel(3L, "SMA", "SMP Muhammadiyah", "SMA Negeri")

        );

        //data request cust model
        CustomerModel model = new CustomerModel(1L, "Reja", addressModels, "Priaa", new Date(), "Surabaya", schoolModels);

        //response data dari repo
        CustomerEntity customerEntity = new CustomerEntity(model);
        List<AddressEntity> addressEntityList = addressModels.stream().map(AddressEntity::new).collect(Collectors.toList());
        customerEntity.setAddress(addressEntityList);

        List<SchoolEntity> schoolEntityList = schoolModels.stream().map(SchoolEntity::new).collect(Collectors.toList());
        customerEntity.setSchools(schoolEntityList);

        //mocking save to DB / validasi
        when(this.repo.save(any(CustomerEntity.class))).thenReturn(customerEntity);
        result = this.service.save(model);
        assertNotNull(result);
        assertEquals("Reja", result.get().getFullName());
        assertEquals("Priaa", result.get().getGender());
        assertEquals("Surabaya", result.get().getPlaceOfBirth());

        //check address
//        assertEquals(2, result.get().getSpecAddress().size());
        assertEquals(3, result.get().getSchools().size());
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}